extends Area2D

func _physics_process(delta): #call this every frame
	var bodies = get_overlapping_bodies()
	for body in bodies: #go through the array of colliding bodies
		if body.name == "Player": #if the collider is the player then kill them
			body.spawnPos = position
