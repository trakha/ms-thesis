#this script controls player movement

extends KinematicBody2D

#CONSTANTS
const UP = Vector2(0, -1)
const FALL_GRAVITY = 5
const FLY_GRAVITY = 10
const DIE_GRAVITY = -100
#VARIABLES (PUBLIC)
export var moveSpeed = 100
export var jumpForce = 300
#VARIABLES (PRIVATE)
var motion = Vector2()
var jumpCounter = 0
var gravity = 20
var spawnPos = Vector2()

func _ready():
	spawnPos = Vector2(position.x, position.y)
	print_debug(position.x)

#function every frame
func _physics_process(delta):
	#push down with force of gravity
	motion.y += gravity

	#right and left movements from input
	if Input.is_action_pressed("ui_right"):
		motion.x = moveSpeed
	elif Input.is_action_pressed("ui_left"):
		motion.x = -moveSpeed
	else: #otherwise do nothing
		motion.x = 0

	#jump implementation from input
	if is_on_floor():
		#gravity = 10 #reset gravity
		if Input.is_action_just_pressed("ui_up"): #initial jump starts counter
			motion.y = -jumpForce
			jumpCounter += 1
			$Sprite.play("Fly")
		else: #reset counter and sprites upon landing
			jumpCounter = 0
			$Sprite.play("Idle")
	#multiple jump implementation from input
	else: 
		if jumpCounter <= 5: #limit number of jumps
			if Input.is_action_just_pressed("ui_up"):
				motion.y = -jumpForce
				jumpCounter += 1
				$Sprite.play("Fly")
		else: #switch to fall sprites
			$Sprite.play("Fall")
			gravity = FALL_GRAVITY
		if Input.is_action_pressed("ui_up"): #give more floaty feel
			gravity = FLY_GRAVITY

	#set player motion based on above calculations
	motion = move_and_slide(motion, UP)
	#check if the player should restart
	check_die()
	#defualt pass escape
	pass
	
func check_die(): #restart player from the correct spawn point
	if position.y < 0:
		position = spawnPos
		gravity = -DIE_GRAVITY

func die(): #when the player dies have them float to the top and restart
	$Sprite.play("Fly")
	gravity = DIE_GRAVITY
