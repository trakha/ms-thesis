extends Area2D

#CONSTANTS
const FLOW_SPEED = -250
#VARIABLES
var timer = 0

func _physics_process(delta): #call this every frame
	var bodies = get_overlapping_bodies()
	for body in bodies: #go through the array of colliding bodies
		if body.name == "Slide": #add a diagonal velocity to the slide 
			body.constant_linear_velocity = Vector2(FLOW_SPEED, -FLOW_SPEED)
