extends Area2D

#CONSTANTS
const SINK_SPEED = .5
#VARIABLES (on start)
onready var treeCollider = get_parent().get_node("FoliageColliPoly")
onready var player = get_parent().get_parent().get_node("Player")
#VARIABLES
var treeColliderPosY
var sinkCounter = 0

func _ready(): #instantiate the tree collider's position value
	treeColliderPosY = treeCollider.position.y

func _physics_process(delta): #call this method every frame
	var bodies = get_overlapping_bodies() #get the array of colliders
	for body in bodies: #go through the array
		if body.name == "Player": #if it's the player, implement sinking
			treeCollider.disabled = false #enable the tree's floor collider
			if player.is_on_floor(): #ensure player is on the tree, and not just in the area
				if sinkCounter <= 50: #only sink a certain distance
					treeCollider.position.y += SINK_SPEED
					sinkCounter += 1
				if sinkCounter > 50: #once the player sinks enough, have them fall through
					treeCollider.disabled = true
					sinkCounter = 0 
					treeCollider.position.y = treeColliderPosY #reset tree collider position
		else: #do nothing for any other body collision
			pass
